## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?: 
* 1268 paquetes.
* ¿Cuánto tiempo dura la captura?:
* 12.81 segundos.
* Aproximadamente, ¿cuántos paquetes se envían por segundo?:
* 99 paquetes por segundo aproximadamente (1268 paquetes / 12.81 segundos).
* ¿Qué protocolos de nivel de red aparecen en la captura?:
* IPV4.
* ¿Qué protocolos de nivel de transporte aparecen en la captura?:
* UDP.
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?:
* RTP.
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N): 7
* Dirección IP de la máquina A: 
* 216.234.64.16.
* Dirección IP de la máquina B: 
* 192.168.0.10.
* Puerto UDP desde el que se envía el paquete:
* 54550.
* Puerto UDP hacia el que se envía el paquete:
* 49154.
* SSRC del paquete:
* 0x31BE1E0E
* Tipo de datos en el paquete RTP (según indica el campo correspondiente):
* ITU -T G.711 PCMU (0)
## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?:
* 4
* ¿Cuántos paquetes hay en el flujo F?:
* 626 paquetes.
* ¿El origen del flujo F es la máquina A o B?:
* A: 216.234.64.16. 
* ¿Cuál es el puerto UDP de destino del flujo F?:
* 49154
* ¿Cuántos segundos dura el flujo F?:
* 12.49 segundos.
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?:
* 21.187000 ms
* ¿Cuál es el jitter medio del flujo?:
* 0.229065 ms
* ¿Cuántos paquetes del flujo se han perdido?:
* 0 paquetes.
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?:
* 0.5 segundos.
## Ejercicio 7. Métricas de un paquete RTP
* ¿Cuál es su número de secuencia dentro del flujo RTP?:
* 18439
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?:
* Tarde, porque los siguientes paquetes tardan mucho menos que el mio.
* ¿Qué jitter se ha calculado para ese paquete?
* 0,062677
* ¿Qué timestamp tiene ese paquete?
* 1769306123
* ¿Por qué número hexadecimal empieza sus datos de audio?
* 0x31BE1E0E
## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`:
* Oct 16, 2023 10:41:19
* Número total de paquetes en la captura:
* 999 paquetes.
* Duración total de la captura (en segundos):
* 9.018292661 segundos.
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?:
* 450 paquetes.
* ¿Cuál es el SSRC del flujo?:
* 0x901b128b
* ¿En qué momento (en ms) de la traza comienza el flujo?
* 0.196449 ms.
* ¿Qué número de secuencia tiene el primer paquete del flujo?
* 25812
* ¿Cuál es el jitter medio del flujo?:
* 0 ms
* ¿Cuántos paquetes del flujo se han perdido?:
* 0 paquetes.
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?:
* pgarcia
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura):
* 1268 - 900= 368 paquetes menos.
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?:
* Tienen los mismos paquetes (450 paquetes).
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?:
* El mio es 25812 y el suyo el número 20053. 25812 - 20053 = 5759.
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?:
* Mi captura dura 9.0182 segundos y 2.9993 segundos por lo tanto la  mía dura bastante más. Mi flujo no comienza hasta los 0.1964 ms y la suya empieza desde el primer paquete que se manda en el instante 0. También cambia el src y el timestamp.
